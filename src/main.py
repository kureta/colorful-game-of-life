#!/usr/bin/env python

from PIL import Image
# import colorsys
import operator


class GameOfLife:
    def __init__(self, _initial_pic):
        self.width = _initial_pic.size[0]
        self.height = _initial_pic.size[1]
        self.pixels = _initial_pic.load()
        self.output_pic = Image.new('RGB', (self.width, self.height), "white")
        self.output_pixels = self.output_pic.load()

        self.current_states = [[[None for row in range(self.height)] for col in range(self.width)] for index in range(24)]
        self.next_states = [[[None for row in range(self.height)] for col in range(self.width)] for index in range(24)]

        for y in range(self.height):
            for x in range(self.width):
                temp_states = self.color_to_binary(self.pixels[x, y])
                for idx in range(24):
                    self.current_states[idx][x][y] = temp_states[idx]

    # convert color to list of binary numbers
    @staticmethod
    def color_to_binary(color):
        bin_list = []
        for val in color:
            bin_list += [int(x) for x in list('{0:08b}'.format(val))]
        return bin_list

    @staticmethod
    def binary_to_color(binary):
        return int(''.join(map(str, binary[0:8])), 2), \
            int(''.join(map(str, binary[8:16])), 2), \
            int(''.join(map(str, binary[16:24])), 2)

    @staticmethod
    def equalize(im):
        h = im.convert("L").histogram()
        lut = []
        for b in range(0, len(h), 256):
            # step size
            step = reduce(operator.add, h[b:b + 256]) / 255
            # create equalization lookup table
            n = 0
            for idx in range(256):
                lut.append(n / step)
                n = n + h[idx + b]
        # map image through lookup table
        return im.point(lut * 3)

    def one_step(self):
        for idx in range(24):
            self.process(idx)

    def save_image(self, name):
        for y in range(self.height):
            for x in range(self.width):
                temp_color = []
                for idx in range(24):
                    temp_color.append(self.current_states[idx][x][y])
                self.output_pixels[x, y] = self.binary_to_color(temp_color)
        # self.equalize(self.output_pic).save("../output/" + name + ".jpg")
        self.output_pic.save("../output/" + name + ".jpg")

    # Apply the 4 rules to the matrix
    def process(self, idx):
        for y in range(self.height):
            for x in range(self.width):
                lives = self.live_neighbors(x, y, idx)
                # Rule 1 - Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                if self.current_states[idx][x][y] == 1 and lives < 2:
                    self.next_states[idx][x][y] = 0
                # Rule 2 - Any live cell with two or three live neighbours lives on to the next generation.
                if self.current_states[idx][x][y] == 1 and (lives == 2 or lives == 3):
                    self.next_states[idx][x][y] = 1
                # Rule 3 - Any live cell with more than three live neighbours dies, as if by overcrowding.
                if self.current_states[idx][x][y] == 1 and lives > 3:
                    self.next_states[idx][x][y] = 0
                # Rule 4 - Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                if self.current_states[idx][x][y] == 0 and lives == 3:
                    self.next_states[idx][x][y] = 1
                else:
                    self.next_states[idx][x][y] = 0

        for y in range(self.height):
            for x in range(self.width):
                self.current_states[idx][x][y] = self.next_states[idx][x][y]

    # Count live neighbors
    def live_neighbors(self, a, b, idx):
        lives = 0
        try:
            if self.current_states[idx][a - 1][b + 1] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a][b + 1] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b + 1] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a - 1][b] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a - 1][b - 1] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a][b - 1] == 1:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b - 1] == 1:
                lives += 1
        except IndexError:
            pass
        return lives


location = "../data/egg.jpg"
input_pic = Image.open(location)

test = GameOfLife(input_pic)
# test.save_image("test")
for i in range(100):
    test.one_step()
    test.save_image(str(i).zfill(2))
    print "saved " + str(i)
