#!/usr/bin/env python

from PIL import Image
# import colorsys
import operator


class GameOfLife:
    def __init__(self, _initial_pic):
        self.width = _initial_pic.size[0]
        self.height = _initial_pic.size[1]
        self.pixels = _initial_pic.load()
        self.output_pic = Image.new('RGB', (self.width, self.height), "white")
        self.output_pixels = self.output_pic.load()

        self.current_states = [[[None for row in range(self.height)] for col in range(self.width)] for index in range(3)]
        self.next_states = [[[None for row in range(self.height)] for col in range(self.width)] for index in range(3)]

        for y in range(self.height):
            for x in range(self.width):
                for idx in range(3):
                    self.current_states[idx][x][y] = self.pixels[x, y][idx]
                    self.next_states[idx][x][y] = self.current_states[idx][x][y]

    def one_step(self):
        for idx in range(3):
            self.process(idx)

    def save_image(self, name):
        for y in range(self.height):
            for x in range(self.width):
                temp_color = (self.current_states[0][x][y], self.current_states[1][x][y], self.current_states[2][x][y])
                self.output_pixels[x, y] = temp_color
        self.output_pic.save("../output/" + name + ".jpg")

    # Apply the 4 rules to the matrix
    def process(self, idx):
        for y in range(self.height):
            for x in range(self.width):
                lives = self.live_neighbors(x, y, idx)
                # Rule 1 - Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                if self.current_states[idx][x][y] > 0 and lives < 2:
                    self.next_states[idx][x][y] = self.current_states[idx][x][y] - 10
                # Rule 2 - Any live cell with two or three live neighbours lives on to the next generation.
                if self.current_states[idx][x][y] > 0 and (lives == 2 or lives == 3):
                    self.next_states[idx][x][y] = 255       # self.current_states[idx][x][y]
                # Rule 3 - Any live cell with more than three live neighbours dies, as if by overcrowding.
                if self.current_states[idx][x][y] > 0 and lives > 3:
                    self.next_states[idx][x][y] = self.current_states[idx][x][y] - 10
                # Rule 4 - Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                if self.current_states[idx][x][y] == 0 and lives == 3:
                    self.next_states[idx][x][y] = 255       # self.pixels[x,y][idx]
                else:
                    self.next_states[idx][x][y] = self.current_states[idx][x][y] - 10
                if self.next_states[idx][x][y] < 0:
                    self.next_states[idx][x][y] = 0
                if self.next_states[idx][x][y] > 255:
                    self.next_states[idx][x][y] = 255

        for y in range(self.height):
            for x in range(self.width):
                self.current_states[idx][x][y] = self.next_states[idx][x][y]

    # Count live neighbors
    def live_neighbors(self, a, b, idx):
        lives = 0
        try:
            if self.current_states[idx][a - 1][b + 1] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a][b + 1] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b + 1] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a - 1][b] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a - 1][b - 1] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a][b - 1] > 0:
                lives += 1
        except IndexError:
            pass
        try:
            if self.current_states[idx][a + 1][b - 1] > 0:
                lives += 1
        except IndexError:
            pass
        return lives


location = "../data/bibi.jpg"
input_pic = Image.open(location)

test = GameOfLife(input_pic)
# test.save_image("test")
for i in range(500):
    test.one_step()
    test.save_image(str(i).zfill(2))
    print "saved " + str(i)
